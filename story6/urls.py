from django.urls import include, path

from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.indexEvent, name='indexEvent'), 
    path('addEvent/', views.addEvent, name='addEvent'),
    path('addMember/<str:id>', views.addMember, name='addMember'),
    path('deleteMember/<str:id>', views.deleteMember, name='deleteMember'),
]
