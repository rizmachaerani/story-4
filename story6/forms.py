from django import forms
from .models import Event, Member

class CreateEvent(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['name']

class CreateMember(forms.ModelForm):
    class Meta:
        model = Member
        fields = ['name', 'event']
