from django.test import TestCase, Client
from .models import Event, Member
from .forms import CreateEvent, CreateMember


# Create your tests here.
class Story6UnitTest(TestCase):
    def test_story_6_event_url_exist(self):
        response = Client().get('/event/')
        self.assertEqual(response.status_code, 200)

    def test_story_6_event_is_using_template(self):
        response = Client().get('/event/')
        self.assertTemplateUsed(response, "event.html")

    def test_model_can_create_new_event(self):
        new_event = Event.objects.create(name='pepewe menyenangkan')
        test_all_event = Event.objects.all().count()
        self.assertEqual(test_all_event, 1)

    def test_model_can_create_new_member(self):
        new_event = Event.objects.create(name='pepewe menyenangkan')
        new_member = Member.objects.create(name='memberTest', event=new_event)
        test_all_member = Member.objects.all().count()
        self.assertEqual(test_all_member, 1)
    
    def test_str_model_event(self):
        event = Event.objects.create(name='pepewe menyenangkan')
        self.assertEqual(event.__str__(), 'pepewe menyenangkan')

    def test_str_model_member(self):
        event = Event.objects.create(name='pepewe menyenangkan')
        member = Member.objects.create(name="memberTest", event=event)
        self.assertEqual(member.__str__(), 'memberTest')
    
    def test_story_6_url_add_event_is_exist(self):
        response = Client().get('/event/addEvent/')
        self.assertEqual(response.status_code, 200)

    def test_story_6_add_event_is_using_template(self):
        response = Client().get('/event/addEvent/')
        self.assertTemplateUsed(response, "addEvent.html")

    def test_story_6_url_add_member_exist(self):
        event = Event.objects.create(name='pepewe menyenangkan')
        eventId = event.id
        response = Client().get('/event/addMember/' + str(eventId))
        print(response)
        self.assertEqual(response.status_code, 200)

    def test_story_6_add_member_is_using_template(self):
        event = Event.objects.create(name='pepewe menyenangkan')
        eventId = event.id
        response = Client().get('/event/addMember/'+ str(eventId))
        print(response)
        self.assertTemplateUsed(response, "addMember.html")
    
    def test_valid_form_member(self):
        new_event = Event.objects.create(name='pepewe menyenangkan')
        eventId = new_event.id
        name = "nameMember"
        response = Client().post('/event/addMember/' + str(eventId), {'name': name, 'eventId' : eventId})
        print(response)
        self.assertEqual(response.status_code, 302)

    def test_story_6_delete_url_is_redierect(self):
        new_event = Event.objects.create(name='pepewe menyenangkan')
        new_member = Member.objects.create(name='member', event=new_event)
        memberId = new_member.id
        response = Client().get('/event/deleteMember/' + str(memberId))
        self.assertEqual(response.status_code, 302)

    def test_model_can_delete_new_event(self):
        new_event = Event.objects.create(name='pepewe menyenangkan')
        new_member = Member.objects.create(name='member', event=new_event)
        test_all_member = Member.objects.all().count()
        new_member.delete()
        test_all_member_after_delete = Member.objects.all().count()
        self.assertNotEqual(test_all_member, test_all_member_after_delete)





