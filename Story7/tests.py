from django.test import TestCase, Client

# Create your tests here.
class Story7UnitTest(TestCase):
    def test_story_7_event_url_exist(self):
        response = Client().get('/accordion/')
        self.assertEqual(response.status_code, 200)

    def test_story_7_event_is_using_template(self):
        response = Client().get('/accordion/')
        self.assertTemplateUsed(response, "index.html")
    