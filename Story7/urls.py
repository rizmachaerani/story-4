from django.urls import include, path

from . import views

app_name = 'Story7'

urlpatterns = [
    path('', views.index, name='index'),
]
