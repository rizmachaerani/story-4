from django.forms import ModelForm
from .models import Matkul

class TheForm(ModelForm):
    class Meta:
        model = Matkul
        fields = '__all__'

