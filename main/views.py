from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import TheForm
from .models import Matkul


def home(request):
    return render(request, 'main/home.html')

def experience(request):
    return render(request, 'main/experience.html')

def contact(request):
    return render(request, 'main/contact.html')

def randomFacts(request):
    return render(request, 'main/randomFacts.html')

def MatkulViews(request):
    matkul_items = Matkul.objects.all()
    return render(request, 'main/matkulForm.html', {'matkul' : matkul_items})

def formSave(request):
    if request.method == 'POST':
        form = TheForm(request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.save()
            return HttpResponseRedirect('matakuliah')

    form = TheForm()
    matkul_items = Matkul.objects.all()
    return render(request, 'main/matkulForm.html', {'form' : form, 'matkul' : matkul_items})

def formDelete(request, matkul_id):
    matkul = Matkul.objects.filter(id = matkul_id)
    matkul.delete()

    return redirect('main:matakuliah')


def detailMataKuliah(request, matkul_id):
    matkul= Matkul.objects.get(id = matkul_id)
    print(matkul.name)
    return render(request, 'main/detailMatkul.html', {'matkul' : matkul})