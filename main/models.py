from django.db import models
from django import forms

class Matkul(models.Model):
    name = models.CharField(max_length=225)
    dosen = models.CharField(max_length=225)
    sks = models.IntegerField(blank=False)
    deskripsi = models.CharField(max_length=225)
    semthn = models.CharField(max_length=225)
    kelas = models.CharField(max_length=225)

    def __str__(self):
        return '%s' % self.name