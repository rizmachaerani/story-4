from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('experience', views.experience, name="experience"), 
    path('contact', views.contact, name='contact'),
    path('random-facts', views.randomFacts, name='randomFacts'),
    path('matakuliah', views.formSave, name='matakuliah'),
    path('detailMataKuliah/<int:matkul_id>', views.detailMataKuliah, name='detailMataKuliah'),
    path('delete/<int:matkul_id>', views.formDelete, name='formDelete'),
]
